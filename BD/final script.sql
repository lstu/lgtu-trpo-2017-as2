if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('Controlled') and o.name = 'FK_CONTROLL_CONTROLLE_EMPLOYEE')
alter table Controlled
   drop constraint FK_CONTROLL_CONTROLLE_EMPLOYEE
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('Controlled') and o.name = 'FK_CONTROLL_CONTROLLE_TASK')
alter table Controlled
   drop constraint FK_CONTROLL_CONTROLLE_TASK
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('Employee') and o.name = 'FK_EMPLOYEE_HAVE_EMPL_ORGANIZA')
alter table Employee
   drop constraint FK_EMPLOYEE_HAVE_EMPL_ORGANIZA
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('Employee') and o.name = 'FK_EMPLOYEE_HAVE_ROLE_ROLE')
alter table Employee
   drop constraint FK_EMPLOYEE_HAVE_ROLE_ROLE
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('Message') and o.name = 'FK_MESSAGE_EMP_SEN_EMPLOYEE')
alter table Message
   drop constraint FK_MESSAGE_EMP_SEN_EMPLOYEE
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('Message') and o.name = 'FK_MESSAGE_SEND_TASK')
alter table Message
   drop constraint FK_MESSAGE_SEND_TASK
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('Project') and o.name = 'FK_PROJECT_HAVE_PROJ_ORGANIZA')
alter table Project
   drop constraint FK_PROJECT_HAVE_PROJ_ORGANIZA
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('Task') and o.name = 'FK_TASK_HAVE_STAT_TASK_STA')
alter table Task
   drop constraint FK_TASK_HAVE_STAT_TASK_STA
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('Task') and o.name = 'FK_TASK_SET_PROJECT')
alter table Task
   drop constraint FK_TASK_SET_PROJECT
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('Task_change') and o.name = 'FK_TASK_CHA_CHANGE_TASK')
alter table Task_change
   drop constraint FK_TASK_CHA_CHANGE_TASK
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('Task_file') and o.name = 'FK_TASK_FIL_INCLUDE_TASK')
alter table Task_file
   drop constraint FK_TASK_FIL_INCLUDE_TASK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('Controlled')
            and   name  = 'Controlled_FK'
            and   indid > 0
            and   indid < 255)
   drop index Controlled.Controlled_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('Controlled')
            and   name  = 'Controlled2_FK'
            and   indid > 0
            and   indid < 255)
   drop index Controlled.Controlled2_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Controlled')
            and   type = 'U')
   drop table Controlled
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('Employee')
            and   name  = 'Have_role_FK'
            and   indid > 0
            and   indid < 255)
   drop index Employee.Have_role_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('Employee')
            and   name  = 'Have_employee_FK'
            and   indid > 0
            and   indid < 255)
   drop index Employee.Have_employee_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Employee')
            and   type = 'U')
   drop table Employee
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('Message')
            and   name  = 'Emp_sen_FK'
            and   indid > 0
            and   indid < 255)
   drop index Message.Emp_sen_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('Message')
            and   name  = 'send_FK'
            and   indid > 0
            and   indid < 255)
   drop index Message.send_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Message')
            and   type = 'U')
   drop table Message
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Organization')
            and   type = 'U')
   drop table Organization
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('Project')
            and   name  = 'Have_proj_FK'
            and   indid > 0
            and   indid < 255)
   drop index Project.Have_proj_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Project')
            and   type = 'U')
   drop table Project
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Role')
            and   type = 'U')
   drop table Role
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('Task')
            and   name  = 'set_FK'
            and   indid > 0
            and   indid < 255)
   drop index Task.set_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Task')
            and   type = 'U')
   drop table Task
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('Task_change')
            and   name  = 'Change_FK'
            and   indid > 0
            and   indid < 255)
   drop index Task_change.Change_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Task_change')
            and   type = 'U')
   drop table Task_change
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('Task_file')
            and   name  = 'include_FK'
            and   indid > 0
            and   indid < 255)
   drop index Task_file.include_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Task_file')
            and   type = 'U')
   drop table Task_file
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Task_status')
            and   type = 'U')
   drop table Task_status
go

/*==============================================================*/
/* Table: Controlled                                            */
/*==============================================================*/
create table Controlled (
   Id_employee          int                  not null,
   Id_task              int                  not null,
   constraint PK_CONTROLLED primary key nonclustered (Id_employee, Id_task)
)
go

/*==============================================================*/
/* Index: Controlled2_FK                                        */
/*==============================================================*/
create index Controlled2_FK on Controlled (
Id_task ASC
)
go

/*==============================================================*/
/* Index: Controlled_FK                                         */
/*==============================================================*/
create index Controlled_FK on Controlled (
Id_employee ASC
)
go

/*==============================================================*/
/* Table: Employee                                              */
/*==============================================================*/
create table Employee (
   Id_employee          int                  not null,
   Id_organization      int                  null,
   Id_role              int                  not null,
   Name_employee        varchar(50)          not null,
   Description_employee varchar(100)         not null,
   constraint PK_EMPLOYEE primary key nonclustered (Id_employee)
)
go

/*==============================================================*/
/* Index: Have_employee_FK                                      */
/*==============================================================*/
create index Have_employee_FK on Employee (
Id_organization ASC
)
go

/*==============================================================*/
/* Index: Have_role_FK                                          */
/*==============================================================*/
create index Have_role_FK on Employee (
Id_role ASC
)
go

/*==============================================================*/
/* Table: Message                                               */
/*==============================================================*/
create table Message (
   Id_message           int                  not null,
   Id_task              int                  not null,
   Id_employee          int                  not null,
   Topic                varchar(30)          not null,
   Body_message         varchar(100)         not null,
   Datetime_message     datetime             not null,
   constraint PK_MESSAGE primary key nonclustered (Id_message)
)
go

/*==============================================================*/
/* Index: send_FK                                               */
/*==============================================================*/
create index send_FK on Message (
Id_task ASC
)
go

/*==============================================================*/
/* Index: Emp_sen_FK                                            */
/*==============================================================*/
create index Emp_sen_FK on Message (
Id_employee ASC
)
go

/*==============================================================*/
/* Table: Organization                                          */
/*==============================================================*/
create table Organization (
   Id_organization      int                  not null,
   Name_org             varchar(50)          not null,
   Descr_org            varchar(100)         not null,
   Address              varchar(50)          not null,
   constraint PK_ORGANIZATION primary key nonclustered (Id_organization)
)
go

/*==============================================================*/
/* Table: Project                                               */
/*==============================================================*/
create table Project (
   Id_project           int                  not null,
   Id_organization      int                  not null,
   Name_project         varchar(30)          not null,
   Description_project  varchar(100)         not null,
   Start_project        datetime             not null,
   Stop_project         datetime             not null,
   constraint PK_PROJECT primary key nonclustered (Id_project)
)
go

/*==============================================================*/
/* Index: Have_proj_FK                                          */
/*==============================================================*/
create index Have_proj_FK on Project (
Id_organization ASC
)
go

/*==============================================================*/
/* Table: Role                                                  */
/*==============================================================*/
create table Role (
   Id_role              int                  not null,
   Name_role            varchar(100)         not null,
   constraint PK_ROLE primary key nonclustered (Id_role)
)
go

/*==============================================================*/
/* Table: Task                                                  */
/*==============================================================*/
create table Task (
   Name_task            varchar(50)          not null,
   Description_task     varchar(100)         not null,
   Start_task           datetime             not null,
   Stop_task            datetime             not null,
   Id_task              int                  not null,
   Id_project           int                  not null,
   Id_status            int                  null,
   constraint PK_TASK primary key nonclustered (Id_task)
)
go

/*==============================================================*/
/* Index: set_FK                                                */
/*==============================================================*/
create index set_FK on Task (
Id_project ASC
)
go

/*==============================================================*/
/* Table: Task_change                                           */
/*==============================================================*/
create table Task_change (
   Id_change            int                  not null,
   Id_task              int                  not null,
   Change_DT            datetime             not null,
   Reason               varchar(100)         not null,
   Stard_change         datetime             not null,
   Stop_change          datetime             not null,
   constraint PK_TASK_CHANGE primary key nonclustered (Id_change)
)
go

/*==============================================================*/
/* Index: Change_FK                                             */
/*==============================================================*/
create index Change_FK on Task_change (
Id_task ASC
)
go

/*==============================================================*/
/* Table: Task_file                                             */
/*==============================================================*/
create table Task_file (
   Id_file              int                  not null,
   Id_task              int                  not null,
   Type_file            varchar(100)         not null,
   Size_file            int                  not null,
   file_file            tinyint              not null,
   constraint PK_TASK_FILE primary key nonclustered (Id_file)
)
go

/*==============================================================*/
/* Index: include_FK                                            */
/*==============================================================*/
create index include_FK on Task_file (
Id_task ASC
)
go

/*==============================================================*/
/* Table: Task_status                                           */
/*==============================================================*/
create table Task_status (
   Id_status            int                  not null,
   Id_task              int                  not null,
   name_status          varchar(100)         not null,
   constraint PK_TASK_STATUS primary key nonclustered (Id_status)
)
go

alter table Controlled
   add constraint FK_CONTROLL_CONTROLLE_EMPLOYEE foreign key (Id_employee)
      references Employee (Id_employee)
go

alter table Controlled
   add constraint FK_CONTROLL_CONTROLLE_TASK foreign key (Id_task)
      references Task (Id_task)
go

alter table Employee
   add constraint FK_EMPLOYEE_HAVE_EMPL_ORGANIZA foreign key (Id_organization)
      references Organization (Id_organization)
go

alter table Employee
   add constraint FK_EMPLOYEE_HAVE_ROLE_ROLE foreign key (Id_role)
      references Role (Id_role)
go

alter table Message
   add constraint FK_MESSAGE_EMP_SEN_EMPLOYEE foreign key (Id_employee)
      references Employee (Id_employee)
go

alter table Message
   add constraint FK_MESSAGE_SEND_TASK foreign key (Id_task)
      references Task (Id_task)
go

alter table Project
   add constraint FK_PROJECT_HAVE_PROJ_ORGANIZA foreign key (Id_organization)
      references Organization (Id_organization)
go

alter table Task
   add constraint FK_TASK_HAVE_STAT_TASK_STA foreign key (Id_status)
      references Task_status (Id_status)
go

alter table Task
   add constraint FK_TASK_SET_PROJECT foreign key (Id_project)
      references Project (Id_project)
go

alter table Task_change
   add constraint FK_TASK_CHA_CHANGE_TASK foreign key (Id_task)
      references Task (Id_task)
go

alter table Task_file
   add constraint FK_TASK_FIL_INCLUDE_TASK foreign key (Id_task)
      references Task (Id_task)
go

'use strict';

exports.authPOST = function(args, res, next) {
  /**
   * Authentication
   * Returns API access token on successful authentication.
   *
   * login String Login
   * password String md5 hash of password
   * no response value expected for this operation
   **/
  res.end();
}

exports.projectGET = function(args, res, next) {
  /**
   * Projects
   * Returns all projects user takes part in. 
   *
   * returns List
   **/
  var examples = {};
  examples['application/json'] = [ {
  "date_start" : 123,
  "project_id" : 123,
  "name" : "aeiou",
  "description" : "aeiou",
  "date_end" : 123
} ];
  if (Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  } else {
    res.end();
  }
}

exports.projectidLogGET = function(args, res, next) {
  /**
   * Logs of messages.
   * Returns full logs of project or one of its tasks. 
   *
   * task Integer Id of task inside the project. If not specified, endpoint returns logs for all of its tasks. (optional)
   * returns List
   **/
  var examples = {};
  examples['application/json'] = [ {
  "topic" : "aeiou",
  "text" : "aeiou",
  "type" : "aeiou",
  "timestamp" : "aeiou"
} ];
  if (Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  } else {
    res.end();
  }
}

exports.projectidLogMessagePOST = function(args, res, next) {
  /**
   * Send a message.
   * Sends a message to the task or project. 
   *
   * text String Text of the message
   * task Integer ID of task to send message to. If not specifies, message sent to project. (optional)
   * file byte[] File to send with the message (optional)
   * returns List
   **/
  var examples = {};
  examples['application/json'] = [ {
  "topic" : "aeiou",
  "text" : "aeiou",
  "type" : "aeiou",
  "timestamp" : "aeiou"
} ];
  if (Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  } else {
    res.end();
  }
}

exports.projectidTaskGET = function(args, res, next) {
  /**
   * Tasks
   * Returns tasks of given project. 
   *
   * scope Integer Filter of tasks. 1 for tasks where user takes part in, 2 for tasks user is responsible for. (optional)
   * returns List
   **/
  var examples = {};
  examples['application/json'] = [ {
  "date_start" : 123,
  "name" : "aeiou",
  "description" : "aeiou",
  "task_id" : 123,
  "date_end" : 123,
  "status" : "aeiou"
} ];
  if (Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  } else {
    res.end();
  }
}

exports.taskidStatusPOST = function(args, res, next) {
  /**
   * Status
   * Change status of the task 
   *
   * status String New status of task task.done, task.paused or task.processing.
   * text String Reason for changing status. (optional)
   * no response value expected for this operation
   **/
  res.end();
}


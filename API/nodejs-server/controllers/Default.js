'use strict';

var url = require('url');

var Default = require('./DefaultService');

module.exports.authPOST = function authPOST (req, res, next) {
  Default.authPOST(req.swagger.params, res, next);
};

module.exports.projectGET = function projectGET (req, res, next) {
  Default.projectGET(req.swagger.params, res, next);
};

module.exports.projectidLogGET = function projectidLogGET (req, res, next) {
  Default.projectidLogGET(req.swagger.params, res, next);
};

module.exports.projectidLogMessagePOST = function projectidLogMessagePOST (req, res, next) {
  Default.projectidLogMessagePOST(req.swagger.params, res, next);
};

module.exports.projectidTaskGET = function projectidTaskGET (req, res, next) {
  Default.projectidTaskGET(req.swagger.params, res, next);
};

module.exports.taskidStatusPOST = function taskidStatusPOST (req, res, next) {
  Default.taskidStatusPOST(req.swagger.params, res, next);
};

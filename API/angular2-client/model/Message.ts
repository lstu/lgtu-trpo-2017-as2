/**
 * API
 * All requests should have additional header \"token\" with token received by /auth endpoint.
 *
 * OpenAPI spec version: 0.0.1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

import * as models from './models';

export interface Message {
    /**
     * Type of the message. task.done, task.paused or task.processing if its message about status changing, message if its a regular message.
     */
    type?: string;

    /**
     * Topic of the message. Returned only if type is message.
     */
    topic?: string;

    /**
     * Text of the message or reason of status changing.
     */
    text?: string;

    /**
     * Unix timestamp of the messaage posting time.
     */
    timestamp?: string;

}
